import 'package:flutter/material.dart';
import 'package:flutter_app/main.dart';
import 'result.dart';
class ThirdPage extends StatelessWidget{
  var answer1,answer2;
  ThirdPage(this.answer1,this.answer2);  // we use this to pass the answer of firstPage and SecondPage to ThirdPage
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Question-3"),
          backgroundColor: Colors.amber,
        ),
        body: Center(
            child: Container(
              padding: EdgeInsets.all(0),
              child: QuestionBox(answer1,answer2),
            )));
  }
}
class QuestionBox extends StatefulWidget{
  var answerFromPageOne,answerFromPageTwo;       // we use this to can use the answer1 and answer2 in thirdpage class in the body of this class when calling the function goToNextPage()
  QuestionBox(this.answerFromPageOne,this.answerFromPageTwo);
  @override
  _BoxState createState() =>  _BoxState(answerFromPageOne,answerFromPageTwo);
}
class _BoxState extends State<QuestionBox> {
  var answerFromPageOne,answerFromPageTwo;
  _BoxState(this.answerFromPageOne,this.answerFromPageTwo);
  var content;  // ="you dont confirm any answer yet";// this is the content of the button confirmation
  int selectedRadio;
  bool isButtonDisabled;
  @override
  void initState(){
    super.initState();
    content="you dont confirm any answer yet";
    selectedRadio=0;
    isButtonDisabled=true;
  }
  setSelectedRadion(int val){
    setState((){
      selectedRadio = val;
    });
  }
  setButtonEnable(){
    setState((){
      isButtonDisabled = false;
    });
  }
  void selectFirstAnswer() {
    setState( () {
      content = "confirm";
    });
  }
  void selectSecondAnswer() {
    setState( () {
      content = "confirm";
    });
  }
  void selectThirdAnswer() {
    setState( () {
      content = "confirm";
    });
  }
  void selectFourthAnswer() {
    setState( () {
      content = "confirm";
    });
  }
  void goToNextPage(){
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ResultPage(answerFromPageOne,answerFromPageTwo,selectedRadio),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return  ListView(
        children: [
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 50),
              child: Text(
                'Third Question',
                style: TextStyle(
                    color: Colors.amber,
                    fontWeight: FontWeight.w500,
                    fontSize: 30),
              )),
          new Column(
              children:[
                Container(
                    padding: EdgeInsets.all(10),
                    child:
                      new Text(
                        "we put here the third question we want to ask",
                        style: new TextStyle(fontSize: 20.0),
                      )

                ),
                new Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Radio(
                        value: 1,
                        groupValue: selectedRadio,     // groupvalue must be the same of radio value
                        onChanged: (val) {
                          selectFirstAnswer();
                          setSelectedRadion(val);
                          setButtonEnable();
                        }
                    ),
                    new Text(
                      '1- first answer',
                      style: new TextStyle(fontSize: 18.0),
                    ),],),
                new Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Radio(
                        value: 2,
                        groupValue: selectedRadio,
                        onChanged: (val) {
                          selectSecondAnswer();
                          setSelectedRadion(val);
                          setButtonEnable();
                        }
                    ),
                    new Text(
                      '2- second answer',
                      style: new TextStyle(fontSize: 18.0),
                    ),],),
                new Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Radio(
                        value: 3,
                        groupValue: selectedRadio,
                        onChanged: (val) {
                          selectThirdAnswer();
                          setSelectedRadion(val);
                          setButtonEnable();
                        }
                    ),
                    new Text(
                      '3- third answer',
                      style: new TextStyle(fontSize: 18.0),
                    ),],),
                new Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Radio(
                        value: 4,
                        groupValue: selectedRadio,
                        onChanged: (val) {
                          selectFourthAnswer();
                          setSelectedRadion(val);
                          setButtonEnable();
                        }
                    ),
                    new Text(
                      '4- fourth answer',
                      style: new TextStyle(fontSize: 18.0),
                    ),],),
                RaisedButton(
                  child: Text(isButtonDisabled?"pick answer":content),
                  onPressed: isButtonDisabled ? null : goToNextPage,
                  color: Colors.teal,
                  textColor: Colors.white,
                  splashColor: Colors.grey,
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),

                )
              ])]);
  }
}
