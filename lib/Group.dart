class Group {
  final int id;
  final int answer1;
  final int answer2;
  final int answer3;
  static final columns = ["id", "answer1", "answer2", "answer3" ];
  Group(this.id, this.answer1, this.answer2, this.answer3);
  factory Group.fromMap(Map<String, dynamic> data) {
    return Group(
      data['id'],
      data['answer1'],
      data['answer2'],
      data['answer3'],
    );
  }
  Map<String, dynamic> toMap() => {
    "id": id,
    "answer1": answer1,
    "answer2": answer2,
    "answer3": answer3,
  };
}
